import userValidation from '../validations/validation';
const bcrypt = require('bcrypt')
const shemaAuth = require('../models/models');
const jwt = require('jsonwebtoken')
require('dotenv').config();


export const signUp = (req,res)=>{
    //recuperation des donnees
    const {body} = req

    //validation des donnees
    const {error} = userValidation(body).validationSignUp;
    if (error) return res.status(404).json('Error validation : ' +error.message);

    //hash du mot de passe
    bcrypt.hash(body.password,10)
    .then(hash=>{
        if (!hash) return res.status(404).json('password not found');

        //suppression du mot de passe et sauvegarde de nouvelles donnees
        delete body.password;
        new shemaAuth({firstname: body.firstname, lastname: body.lastname, email: body.email, password: hash})
        .save()
        .then( user => {
            res.status(200).json(user);
            console.log(user)
        })
        .catch(err=> res.status(500).json(err.message))
    })
    .catch(err => res.status(401).json(err.message))

}

export const login = (req,res)=>{
    //recuperation des donnees
    const {email, password} = req.body;

    //validation des donnees
    const {error} = userValidation(req.body).validationLogin;
    if (error) return res.json('Error validation : ' + error.message)

    //trouver l'utilisateur dans la BDD
    shemaAuth.findOne({email : email})
    .then(user=>{
        if(!user) return res.status(404).json({msg:"cet utilisateur n'existe pas"})
    //comparer le mot de passe
    bcrypt.compare(password, user.password)
    .then(match=>{
        if(!match) return res.status(401).json("error password");
        // shemaAuth.findOne({password : password})
        // .then(()=>{
            res.status(200).json({
                email: user.email,
                id : user._id,
                token : jwt.sign({id: user._id},process.env.SECRET_KEY,{expiresIn:'12h'})
            })
        // })
        .catch(err=> console.log("mot de passe not found " + err.message))
    })
    .catch(err => res.status(404).json(err.message)) 
    })
    .catch(err => res.json("Error connexion " + err.message))
}