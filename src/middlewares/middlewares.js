const passport = require('passport');
const passportJWT = require('passport-jwt');
require('dotenv').config();
const shemaAuth = require('../models/models');


passport.use(
    //passport nouvelle strategy 
    new passportJWT.Strategy({
        jwtFromRequest : passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey : process.env.SECRET_KEY
    }, function (jwtPayload,done){
        return shemaAuth.findById(jwtPayload.id)
        .then(user => done(null, user))
        .catch(err => done(err))
    }
    )
)