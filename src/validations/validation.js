const joi = require('joi')

const userValidation = (body) =>{
    const validationSignUp = joi.object({
        firstname : joi.string().min(2).max(30).trim().required(),
        lastname : joi.string().min(2).max(30).trim().required(),
        email : joi.string().email().trim().required(),
        password : joi.string().min(8).max(30).required(),
    });
    const validationLogin = joi.object({
        email : joi.string().email().required(),
        password : joi.string().min(8).max(30).required(),
    });
    return{
        validationSignUp : validationSignUp.validate(body),
        validationLogin : validationLogin.validate(body),

    }
}

export default userValidation;