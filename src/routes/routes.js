import { signUp, login } from "../controllers/controllers";
const passport = require('passport')

const routes = (app) =>{
    app.route('/signUp')
    .post(signUp)

    app.route('/login')
    .post(login)

    app.use(passport.authenticate("jwt",{session:false}))
    app.route('/proteger')
    .get((req,res)=>{
        res.send('proteger')
    })
}
export default routes;