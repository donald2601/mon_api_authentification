const mongoose = require('mongoose')
const muv = require('mongoose-unique-validator')

const shemaAuth = mongoose.Schema({
    firstname : {type : String, required:true},
    lastname : {type : String, required:true},
    email : {type : String, required: true, unique: true},
    password : {type : String, required: true},
})
mongoose.plugin(muv);

module.exports = mongoose.model('authentification',shemaAuth)