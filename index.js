require('./src/middlewares/middlewares');
//ROUTES
const express = require('express')
const app = express();

//CORS
const cors = require('cors')
app.use(cors())

//MONGOOSE CONNEXION
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/authentification', { useNewUrlParser: true })
.then(()=> console.log('connecter avec success'))
.catch(err => console.error(err));

//BODY-PARSER
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

//ROUTES
import routes from './src/routes/routes'
routes(app);

//SERVER
const PORT = 8080;
app.listen(PORT,()=>{
    console.log('server pret');
})